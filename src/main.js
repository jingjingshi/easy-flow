// The Vue build version to load with the `import` command
// (runtime-only or standalone) has been set in webpack.base.conf with an alias.
import Vue from 'vue'
import router from './router'
import ElementUI from 'element-ui'
import {codemirror} from 'vue-codemirror'
import VueDraggable from 'vuedraggable'

import 'element-ui/lib/theme-chalk/index.css'
import 'codemirror/lib/codemirror.css'

const Components = [
  VueDraggable
]

const install = () => {
  Components.map(x => Vue.component(x.name, x))
}

Vue.config.productionTip = false
Vue.use(ElementUI, {size: 'small'})
Vue.use(VueDraggable)
Vue.use(codemirror)

install()

/* eslint-disable no-new */
new Vue({
  router
}).$mount('#app')
