export const Anchors = ['TopCenter', 'TopRight', 'RightMiddle', 'BottomRight', 'Bottom', 'BottomLeft', 'LeftMiddle', 'TopLeft']

/// 定义锚点的位置
const defaultAnchors = ['Top', 'Right', 'Bottom', 'Left', [0.25, 0, 0, -1], [0.75, 0, 0, -1], [0.25, 1, 0, 1], [0.75, 1, 0, 1],
  [0, 0.25, 0, -1], [0, 0.75, 0, -1], [1, 0.25, 0, 1], [1, 0.75, 0, 1]]
export const jsPlumbConfig = {
  Anchor: [defaultAnchors], // 多个锚点位置声明
  Connector: ['Flowchart'],
  ConnectionOverlays: [
    [
      'Custom', {
        create: function (component) {
          const div = document.createElement('div')
          div.setAttribute('class', 'overlays__target')
          return div
        },
        location: 0.05
      }
    ],
    [
      'Custom', {
        create: function (component) {
          const div = document.createElement('div')
          div.setAttribute('class', 'overlays__end')
          return div
        },
        location: 0.95
      }
    ]
    // [
    //   'Custom',
    //   {
    //     create: function (component) {
    //       const div = document.createElement('div')
    //       div.setAttribute('class', 'overlays--default')
    //       return div
    //     },
    //     location: 0.3
    //   }
    // ]
  ],
  // 通过jsPlumb.draggable拖拽元素时的默认参数设置
  DragOptions: {cursor: 'pointer', zIndex: 2000},
  DropOptions: {}, // target Endpoint放置时的默认参数设置
  ConnectionsDetachable: true, // 连接是否可以使用鼠标默认分离
  Container: 'jsplumb-preview',
  Endpoint: ['Dot', {radius: 7}],
  EndpointStyle: { fill: '#fff', outlineStroke: '#AAAAAA', strokeWidth: 2 }, // 端点样式
  EndpointHoverStyle: {fill: '#fff', outlineStroke: '#4478E0', stroke: '#4478E0'},
  HoverPaintStyle: { strokeWidth: 4, dashstyle: 'none', stroke: '#4478E0' },
  LabelStyle: { color: 'black' }, // 标签样式
  LogEnabled: true,
  MaxConnections: -1, // 端点最大连接线 无数个链接
  PaintStyle: { stroke: '#10151F', strokeWidth: 4, dashstyle: '3 3' }, // 连接线的样式
  ReattachConnections: true, // 端点是否可以再次重新链接
  RenderMode: 'svg' // 默认渲染模式
}

// jsPlumb的锚点分为四类

// Static 静态 固定位置的锚点
// Dynamic jsPlumb自动选择合适的锚点，动态锚点
// Perimeter 边缘锚点，会根据节点形状去改变位置
// Continuous 根据节点位置，自动调整位置的锚点

export const jsPlumbSourceOptions = {
  filter: '.flow-node-drag',
  filterExclude: false,
  anchor: 'AutoDefault', // 根据节点位置，自动调整位置的锚点
  // allowLoopback: true,
  reattach: true
  // EndpointStyle: {fill: 'red', outlineStroke: 'green'}
}

export const jsPlumbTargetOptions = {
  filter: '.flow-node-drag',
  filterExclude: false,
  anchor: 'AutoDefault',
  // allowLoopback: true,
  reattach: true
}

export const createJsPlumbConnectOptions = function ({from, to, label}, ctx) {
  return {
    // isSource: true,
    // isTarget: true,
    deleteEndpointsOnDetach: false,
    // detachable: false,
    overlays: [
      [
        'Label', {
          label: label.text,
          visible: !!label.text,
          cssClass: `label--default`,
          location: 0.5,
          id: label.id,
          events: {
            click: function (labelOverlay, originalEvent) {
              // console.log(labelOverlay, originalEvent, labelOverlay.getLabel(), labelOverlay.id)
              const labelText = '自定义label 修改'
              labelOverlay.setLabel(labelText)
              label.text = labelText
              ctx.$message({
                showClose: true,
                message: '修改成功',
                type: 'success'
              })
            }
          }
        }
      ]
    ]
  }
}
