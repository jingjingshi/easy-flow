export const menus = [
  {id: 'entity', name: '实体', type: 'entity'},
  {id: 'line1', name: '连线1', type: 'line1'},
  {id: 'line2', name: '连线2', type: 'line2'},
  {id: 'group', name: '分组', type: 'group'}
]

export const nodeList = [
  {
    id: `entity0`,
    name: `节点_1`,
    left: 100,
    top: 100,
    zIndex: 1,
    type: 'entity'
  },
  {
    id: `entity1`,
    name: `节点_2`,
    left: 900,
    top: 150,
    zIndex: 2,
    type: 'entity'
  }
]

export const lineList = [
  {from: 'entity0', to: 'entity1', type: 'entity-entity', label: {text: '自定义label1', id: 'label-entity0-entity1'}, uuids: ['entity0_TopCenter', 'entity1_Bottom']}
]
