import Vue from 'vue'
import VueRouter from 'vue-router'
import App from './App'
import Mine from './components/mine/index'

Vue.use(VueRouter)

const routes = [
  {path: '/', component: App},
  {path: '/mine', component: Mine}
]

export default new VueRouter({
  routes
})
